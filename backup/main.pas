unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Buttons,
  ExtCtrls, DCPdes, DCPrijndael, DCPsha1, crc;

type

  { TFormMain }

  TFormMain = class(TForm)
    btnAbrirDec: TBitBtn;
    btnAbrirEnc: TBitBtn;
    btnEnc: TBitBtn;
    btnDec: TBitBtn;
    aescipher: TDCP_rijndael;
    descipher: TDCP_des;
    abrir: TOpenDialog;
    decFile: TLabeledEdit;
    encFile: TLabeledEdit;
    guardar: TSaveDialog;
    procedure btnAbrirEncClick(Sender: TObject);
    procedure btnDecClick(Sender: TObject);
    procedure btnEncClick(Sender: TObject);
    procedure encriptar( origen, destino : tfileStream; clave : string );
    procedure decriptar( origen, destino : tfileStream; clave : string );
    procedure btnAbrirDecClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  FormMain: TFormMain;

implementation

{$R *.lfm}

{ TFormMain }

function crcFile(filename: string): string;
var
   crcvalue: longword;
   fin: File;
   NumRead: Word;
   buf: Array[1..2048] of byte;
begin
     crcvalue := crc32(0,nil,0);
     AssignFile (fin, filename);
     Reset (Fin,1);
     repeat
           BlockRead(fin, buf, Sizeof(buf), NumRead);
           crcvalue := crc32(crcvalue, @buf[1], NumRead);
     until (NumRead=0);
     CloseFile(fin);
     result := crcvalue.ToHexString;
end;



procedure tformMain.encriptar( origen, destino : tfileStream; clave : string );
begin
     aescipher.InitStr( clave , TDCP_sha1 );
     aescipher.EncryptStream( origen, destino, origen.Size );
     aescipher.Burn;
     aescipher.free;
end;

procedure tformMain.decriptar( origen, destino : tfileStream; clave : string );
begin
     aescipher.InitStr( clave, TDCP_sha1 );
     aescipher.DecryptStream( origen, destino, origen.Size );
     aescipher.Burn;
     aescipher.free;
end;

procedure TFormMain.btnEncClick(Sender: TObject);
var
   firstClave, secondClave : string;
   origen, destino : tfileStream;
begin
     guardar.FileName:='file.crypt';
     if fileExists( decFile.Text ) and
        guardar.Execute and
        inputQuery('ingrese clave','ingresar', firstClave) and
        inputQuery('reingrese clave','reingresar',secondClave) and
        (firstClave = secondClave) then
        begin
             origen := tfileStream.Create(decFile.Text,fmOpenRead);
             destino := tfileStream.Create(guardar.FileName, fmCreate);

             encriptar( origen, destino, firstClave );

             origen.Free;
             destino.Free;

             decFile.Clear;
             encFile.Clear;
        end
     else
         showMessage('Error al encriptar')

end;

procedure TFormMain.btnDecClick(Sender: TObject);
var
   firstClave : string;
   origen, destino : tfileStream;
begin
     guardar.FileName:='file.decrypt';
     if fileExists( encFile.Text ) and
        guardar.Execute and
        inputQuery('ingrese clave','ingresar', firstClave) then
        begin
             origen := tfileStream.Create(encFile.Text,fmOpenRead);
             destino := tfileStream.Create(guardar.filename, fmCreate);

             decriptar( origen, destino, firstClave );

             origen.Free;
             destino.Free;

             decFile.Clear;
             encFile.Clear;
        end
     else
         showMessage('Error al desencriptar');
end;

procedure TFormMain.btnAbrirEncClick(Sender: TObject);
begin
     if abrir.Execute then
        begin
             encFile.Text:=abrir.filename;
        end;
end;

procedure TFormMain.btnAbrirDecClick(Sender: TObject);
begin
     if abrir.Execute then
        begin
             decFile.Text:=abrir.filename;
             showMessage(crcFile(abrir.FileName));
        end;
end;

end.

